﻿using CYFang;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoutubeDownloadApp
{
    /// <summary>
    ///
    /// </summary>
    public partial class DownloadProcessBar : Form
    {
        /// <summary>
        ///
        /// </summary>
        private List<AFile> files;

        /// <summary>
        ///
        /// </summary>
        private List<Task> tasks;

        private DownloadProcessBar bar;

        /// <summary>
        ///
        /// </summary>
        public DownloadProcessBar(List<AFile> files, List<Task> tasks)
        {
            InitializeComponent();
            this.files = files;
            this.tasks = tasks;
            Init();
        }

        /// <summary>
        ///
        /// </summary>
        private void Init()
        {
            this.bar = this;
            this.DialogResult = DialogResult.Cancel;

            this.progressBar.Maximum = 1000;
            this.progressBar.Minimum = 0;
            this.progressBar.Step = 1;

            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.DoWork += BackgroundWorker_DoWork;
            this.backgroundWorker.ProgressChanged += BackgroundWorker_ProgressChanged;
            this.backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;
            this.backgroundWorker.RunWorkerAsync();

            this.backgroundWorkerStatus.WorkerSupportsCancellation = true;
            this.backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted1;
            this.backgroundWorkerStatus.DoWork += BackgroundWorkerStatus_DoWork;
            this.backgroundWorkerStatus.RunWorkerAsync();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_RunWorkerCompleted1(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorkerStatus_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int count = 0;
            while (count < files.Count)
            {
                foreach (var f in files)
                {
                    if (f.FileStatus == AFile.Status.DONE || f.FileStatus == AFile.Status.ERROR)
                    {
                        count++;
                    }
                }
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            this.progressBar.Value = e.ProgressPercentage;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            this.backgroundWorker.RunWorkerAsync();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BackgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            for (int index = 0; index <= 1000; index++)
            {
                backgroundWorker.ReportProgress(index);
                Thread.Sleep(10);
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCancel_Click(object sender, System.EventArgs e)
        {
            this.Close();
        }
    }
}