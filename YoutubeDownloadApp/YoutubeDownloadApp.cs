﻿using CYFang;
using CYFang.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YoutubeDownloadApp
{
    public partial class YoutubeDownloadApp : Form
    {
        /// <summary>
        /// Member
        /// </summary>
        public class Member
        {
            /// <summary>
            /// Type
            /// </summary>
            public enum Type
            {
                /// <summary>
                /// Music
                /// </summary>
                Music,

                /// <summary>
                /// Video
                /// </summary>
                Video
            }

            /// <summary>
            /// Title
            /// </summary>
            public String title { get; set; }

            /// <summary>
            /// URL
            /// </summary>
            public String url { get; set; }

            /// <summary>
            /// Type
            /// </summary>
            public Type type { get; set; }
        }

        /// <summary>
        /// Regex
        /// </summary>
        private readonly Regex regex = new Regex(@"^((?:https?:)?\/\/)?((?:www|m)\.)?((?:youtube\.com|youtu.be))(\/(?:[\w\-]+\?v=|embed\/|v\/)?)([\w\-]+)(\S+)?$");

        /// <summary>
        /// Url
        /// </summary>
        private string Url { get { return textBoxUrl.Text; } }

        /// <summary>
        ///
        /// </summary>
        private BindingList<Member> members;

        /// <summary>
        ///
        /// </summary>
        private List<AFile> files;

        /// <summary>
        ///
        /// </summary>
        private List<Task> tasks;

        /// <summary>
        ///
        /// </summary>
        private DownloadProcessBar processBar;

        /// <summary>
        ///
        /// </summary>
        public YoutubeDownloadApp()
        {
            InitializeComponent();
            Init();
        }

        /// <summary>
        /// Init
        /// </summary>
        private void Init()
        {
            if (Directory.Exists(AFile.GetDownloadPath()))
            {
                Directory.CreateDirectory("Download");
            }

            DataGridViewTextBoxColumn columnTitle = new DataGridViewTextBoxColumn()
            {
                HeaderText = "標題",
                DataPropertyName = "title",
                AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            };
            DataGridViewTextBoxColumn columnUrl = new DataGridViewTextBoxColumn()
            {
                HeaderText = "網址",
                DataPropertyName = "url",
            };
            DataGridViewComboBoxColumn columnType = new DataGridViewComboBoxColumn()
            {
                DataSource = Enum.GetValues(typeof(Member.Type)),
                DataPropertyName = "type",
                HeaderText = "種類",
            };
            this.dataGridView.Columns.Add(columnTitle);
            this.dataGridView.Columns.Add(columnUrl);
            this.dataGridView.Columns.Add(columnType);

            this.members = new BindingList<Member>();
            this.bindingSource.DataSource = this.members;
            this.bindingNavigator.BindingSource = this.bindingSource;
            this.dataGridView.DataSource = this.bindingSource;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonAdd_Click(object sender, EventArgs e)
        {
            if (regex.IsMatch(textBoxUrl.Text))
            {
                this.members.Add(new Member
                {
                    title = AFile.GetVideoTitle(Url),
                    url = Url,
                    type = Member.Type.Music
                });
                textBoxUrl.Text = String.Empty;
            }
            else
            {
                MessageBox.Show("請確認網址輸入正確!!");
            }
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonDownload_Click(object sender, EventArgs e)
        {
            this.files = new List<AFile>();
            this.tasks = new List<Task>();

            foreach (var m in members)
            {
                var f = AFile.GetInstance(m);
                this.files.Add(f);
                var t = Task.Factory.StartNew(f.DownloadAsync);
                this.tasks.Add(t);
            }

            processBar = new DownloadProcessBar(files, tasks);
            if (processBar.ShowDialog() == DialogResult.Cancel)
            {
                MessageBox.Show("已取消下載!!");
            }
            else
            {
                MessageBox.Show("下載完成");
                this.members.Clear();
                foreach (var t in tasks)
                {
                    t.Dispose();
                }
                this.tasks.Clear();
            }
        }
    }
}