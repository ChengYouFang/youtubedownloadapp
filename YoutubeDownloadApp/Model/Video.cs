﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using static YoutubeDownloadApp.YoutubeDownloadApp;

namespace CYFang.Model
{
    /// <summary>
    ///
    /// </summary>
    internal class Video : AFile
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="member"></param>
        internal Video(Member member) : base(member) { }

        /// <summary>
        ///
        /// </summary>
        internal async override void DownloadAsync()
        {
            var video = youtube.GetVideo(member.url);
            String fileName = video.FullName.Replace(" - YouTube", "");
            string path = GetFilePath(fileName);
            try
            {
                SetStatus(Status.DOWNLOAD);
                var stream = new FileStream(path, FileMode.Create);
                var data = await video.GetBytesAsync();
                await stream.WriteAsync(data, 0, data.Length);
                await stream.FlushAsync();
                stream.Close();
                data = null;
                stream = null;

                if (video.FileExtension.Contains("webm"))
                {
                    await ConvertWembToMp4(fileName);
                }
                else
                {
                    SetStatus(Status.DONE);
                }
            }
            catch
            {
                SetStatus(Status.ERROR);
            }
            finally
            {
                video = null;
                GC.Collect();
                GC.SuppressFinalize(this);
                GC.WaitForPendingFinalizers();
            }
        }

        /// <summary>
        /// Convert wemb to mp4
        /// </summary>
        /// <param name="fileName"></param>
        private Task ConvertWembToMp4(string fileName)
        {
            var tcs = new TaskCompletionSource<bool>();
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "ffmpeg.exe")
                {
                    CreateNoWindow = true,
                    Arguments = String.Format(" -i \"{0}\" \"{1}\"", GetFilePath(fileName), GetFilePath(fileName.Replace(".webm", ".mp4"))),
                    UseShellExecute = false
                }
            };

            process.Exited += (s, e) =>
            {
                SetStatus(Status.DONE);
                process.Dispose();
                process = null;
                tcs.SetResult(true);
            };
            process.Start();

            return tcs.Task;
        }

        #region Other

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }

        #endregion Other
    }
}