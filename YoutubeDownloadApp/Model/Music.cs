﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using static YoutubeDownloadApp.YoutubeDownloadApp;

namespace CYFang.Model
{
    /// <summary>
    /// Music
    /// </summary>
    internal sealed class Music : AFile
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="member"></param>
        internal Music(Member member) : base(member) { }

        /// <summary>
        /// convert video to mp3
        /// </summary>
        private Task ConvertMp4ToMp3(string fileName)
        {
            var tcs = new TaskCompletionSource<bool>();
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "ffmpeg.exe")
                {
                    CreateNoWindow = true,
                    Arguments = String.Format(" -i \"{0}\" \"{1}\"", GetFilePath(fileName), GetFilePath(fileName.Replace(".mp4", ".mp4"))),
                    UseShellExecute = false
                }
            };

            process.Exited += (s, e) =>
            {
                SetStatus(Status.DONE);
                process.Dispose();
                process = null;
                tcs.SetResult(true);
            };
            process.Start();

            return tcs.Task;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        private Task ConvertWebmToMp3(string fileName)
        {
            var tcs = new TaskCompletionSource<bool>();
            var process = new Process()
            {
                StartInfo = new ProcessStartInfo(AppDomain.CurrentDomain.BaseDirectory + "ffmpeg.exe")
                {
                    CreateNoWindow = true,
                    Arguments = String.Format(" -i \"{0}\" \"{1}\"", GetFilePath(fileName), GetFilePath(fileName.Replace(".webm", ".mp3"))),
                    UseShellExecute = false
                }
            };

            process.Exited += (s, e) =>
            {
                SetStatus(Status.DONE);
                process.Dispose();
                process = null;
                tcs.SetResult(true);
            };
            process.Start();

            return tcs.Task;
        }

        /// <summary>
        /// 下載影片
        /// </summary>
        internal override async void DownloadAsync()
        {
            var video = youtube.GetVideo(member.url);
            String fileName = video.FullName.Replace(" - YouTube", "");
            string path = GetFilePath(fileName);
            try
            {
                SetStatus(Status.DOWNLOAD);
                var stream = new FileStream(path, FileMode.Create);
                var data = await video.GetBytesAsync();
                await stream.WriteAsync(data, 0, data.Length);
                await stream.FlushAsync();
                stream.Close();
                data = null;
                stream = null;
                if (video.FileExtension.Contains("webm"))
                {
                    await ConvertWebmToMp3(fileName);
                }
                else
                {
                    await ConvertMp4ToMp3(fileName);
                }
            }
            catch
            {
                SetStatus(Status.ERROR);
            }
            finally
            {
                video = null;
                GC.Collect();
                GC.SuppressFinalize(this);
                GC.WaitForPendingFinalizers();
            }
        }

        #region Other

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString();
        }

        #endregion Other
    }
}