﻿using CYFang.Model;
using Microsoft.Win32;
using System;
using static YoutubeDownloadApp.YoutubeDownloadApp;
using vl = VideoLibrary;

namespace CYFang
{
    /// <summary>
    /// Abstract file
    /// </summary>
    public abstract class AFile : IDisposable
    {
        /// <summary>
        /// 檔案狀態
        /// </summary>
        public enum Status : int
        {
            /// <summary>
            /// 下載中
            /// </summary>
            DOWNLOAD,

            /// <summary>
            /// 完成
            /// </summary>
            DONE,

            /// <summary>
            /// 錯誤
            /// </summary>
            ERROR,

            /// <summary>
            /// 轉檔中
            /// </summary>
            CONVERT
        }

        /// <summary>
        ///
        /// </summary>
        protected static readonly vl.YouTube youtube = vl.YouTube.Default;

        /// <summary>
        /// Member
        /// </summary>
        protected Member member;

        /// <summary>
        /// File status
        /// </summary>
        public Status FileStatus { private set; get; }

        /// <summary>
        ///
        /// </summary>
        /// <param name="member"></param>
        public AFile(Member member) { this.member = member; }

        /// <summary>
        /// Download async
        /// </summary>
        internal abstract void DownloadAsync();

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            if (member != null)
            {
                member = null;
            }
        }

        /// <summary>
        /// Get an instance
        /// </summary>
        /// <param name="url">url</param>
        /// <param name="isMusic">music/video</param>
        /// <returns></returns>
        public static AFile GetInstance(Member member)
        {
            if (member.type == Member.Type.Music)
                return new Music(member);
            else
                return new Video(member);
        }

        /// <summary>
        /// set file status
        /// </summary>
        /// <param name="status">status</param>
        protected void SetStatus(Status status)
        {
            this.FileStatus = status;
        }

        /*
        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        static internal String GetDownloadPath()
        {
            return Registry.GetValue(@"HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "{374DE290-123F-4565-9164-39C4925E467B}", String.Empty).ToString();
        }
        */

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public static String GetDownloadPath()
        {
            return String.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "Download");
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        static internal String GetFilePath(String fileName)
        {
            return String.Format(@"{0}\{1}", GetDownloadPath(), fileName);
        }

        /// <summary>
        /// Get video title
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static String GetVideoTitle(String url)
        {
            try
            {
                return youtube.GetVideo(url).Title.Replace(" - YouTube", "");
            }
            catch
            {
            }

            return string.Empty;
        }
    }
}